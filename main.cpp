#include <iostream>
#include <cmath>
#include <cstdlib>
#include <time.h>
#include <synchapi.h>

using namespace std;

int main() {
    //Zadanie z funkcją SRAND z arkusza
    /*
        Zadanie lekko dlugie, lecz staralem sie zapewnic czytelnosc
    */
    srand(time(NULL));
    int number, maxScope, sum, randomNumber, counter;
    cout << "Podaj poczatkowa liczbe: " << endl;
    cin >> number;
    cout << "Podaj maksymalny zakres losowania liczby: " << endl;
    cin >> maxScope;
    sum = 0;
    counter = 0;
    do {
        randomNumber = rand() % maxScope + 1;
        cout << "Wylosowana liczba: " << randomNumber << endl;
        sum += randomNumber;
        counter++;
    } while (sum < number);
    cout << "Suma wylosowanych liczb = " << sum << endl;
    cout << "Ilosc wylosowanych liczb = " << counter << endl;
    cout << endl;
    cout << endl;
    cout << endl;
    //Dodatkowe zadanie na wyzsza ocene
    //Proste losowanie liczb kilku liczb czyli losowanie numerkow lotto :)
    int lottoNumber;
    int counterOfNumbers = 0;
    int Tablica[6];
    cout << "Witaj w losowaniu lotto!" << endl;
    Sleep(1500);
    do {
        lottoNumber = rand() % 99;
        cout << "Wylosowana liczba nr " << counterOfNumbers + 1 << ": " << lottoNumber << endl;
        Tablica[counterOfNumbers] = lottoNumber;
        Sleep(1500);
        counterOfNumbers++;
    } while (counterOfNumbers < 6);
    cout << "Oto szczesliwe liczby: ";
    for (int i = 0; i < counterOfNumbers; ++i) {
        cout << Tablica[i] << " ";
    }
    return 0;
}

